import random
import subprocess
import argparse
import re
from collections import namedtuple
import json


def arg():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--interface", dest="interfacePars", help="Interface 2 change mac address")
    parser.add_argument("-m", "--mac", dest="newMacPars", help="New MAC address or if none set random MAC")
    parser.add_argument("-v", "--vendor", dest="vendorID",
                        help="""Set some vendor id? line number in file. Set up it in parameter 
                        -f argument. If file not available generate random file""", type=int)
    parser.add_argument("-s", "--show-vendors", dest="showVendor", action="store_true",
                        help="Show available vendors from file. Set up it in parameter -f argument")
    parser.add_argument("-f", "--vendor-file", dest="vendorFile", help="Json file with vendor list",
                        )
    if not parser.parse_args().interfacePars:
        parser.error("[-] pls specify an interface, use --help or -h for more info")
    else:
        return parser.parse_args()


# Проверка мака на валидность, разбиваем мак на массив по двоеточиям
# и пробуем перобразовать каждый элемент массива из 16 ричного числа в int
# так как все элементы мак адреса это 16 числа то в случае правильного мака
# это сработает
def check_mac(mac):
    macarray = str.split(mac, ':')
    for mac_part in macarray:
        try:
            int(mac_part, 16)
        except Exception:
            return False
    return True


# создаем случайный мак или если указано создаем мак определенного вендора
# Первый октет должен содержать в себе определенную комбинацию байт
# поэтому генерим его отдельно в соответствии со стандартом
# если указан номер вендора из файла то генерируем недостающие октеты
def generate_random_mac(vendor=None, available_vendors=None):
    if vendor:
        # Если указан идентификатор вендора то загружаем его из массива который загружен из файла
        if int(vendor) > len(available_vendors):
            print("Error: to great value vendor id")
            exit(5)
        vendor_octets = available_vendors[int(vendor)][1]
        print("Generate vendor mac " + available_vendors[int(vendor)][0])
    else:
        # Иначе генерим случайный mac
        print("Random mac generate")
        first_oktet = ""
        for i in range(6):
            temp = str(random.randint(0, 1))
            first_oktet += temp
        first_oktet = first_oktet + "10"
        vendor_octets = (hex(int(first_oktet, 2))[2:].zfill(2) + ":"
                         + hex(random.randint(0, 255))[2:].zfill(2) + ":"
                         + hex(random.randint(0, 255))[2:].zfill(2))
    random_mac = (str(vendor_octets))

    # Если в файле указано нечетное количество символов для последнего октета
    # то переводим строку в массив
    # берем последний октет и добавляем к нему переведенное в символ шеснадцатиричное число
    # переводим массив обратно в строку и добавляем случайные значения до того как mac будет готов
    if len(str.split(random_mac, ':')[-1]) % 2 != 0:
        macarray = str.split(random_mac, ':')
        macarray[-1] = str(macarray[-1] + hex(random.randint(0, 15))[2:].zfill(1)).upper()
        random_mac = macarray[0]
        del macarray[0]
        random_mac = random_mac + ''.join(map(lambda x: ":" + x, macarray))
    # длина готово мака 17 символов добавляем отсутствующие октеты пока длина мака не станет равной 17
    while len(random_mac) != 17:
        random_mac += (":" + hex(random.randint(0, 255))[2:].zfill(2))

    return random_mac


# Проверка что интерфейс переданный в параметре действительно существует в системе
def chek_network_interfaces(interface):
    ifconfig = subprocess.check_output("ifconfig").decode()
    name_pattern = "^(\w+):\s"
    pattern = re.compile("".join((name_pattern)), flags=re.MULTILINE)
    interfaces = pattern.findall(ifconfig)
    Interface = namedtuple("Interface", "name")
    if interface in interfaces:
        return True
    return False


# применяем новый мак адрес через субпроцесс
def change_mac(network_interface, valid_newmac):
    print("[+] Changing MAC address for " + network_interface + " to " + valid_newmac + (
        " Fake vendor is " + available_vendors[int(options.vendorID)][0] if options.vendorID else ''))
    print("-> shooting down " + network_interface)
    subprocess.call("sudo ifconfig " + network_interface + " down ", shell=True)
    print("-> changing mac to " + valid_newmac)
    subprocess.call("sudo ifconfig " + network_interface + " hw ether " + valid_newmac, shell=True)
    print("-> powering up " + network_interface)
    subprocess.call("sudo ifconfig " + network_interface + " up", shell=True)
    print("result: ")
    subprocess.call("sudo ifconfig " + network_interface, shell=True)


# Читаем json файл с данными для вендора озвращаем словарь в котором в качестве ключа
# указан номер строки из файла а значение это имя вендора и октет
def dump_files():
    file_name = "macaddress.io-db.json"
    if options.vendorFile:
        file_name = options.vendorFile
    try:
        json_file = open(file_name)
    except IOError:
        print("Error: File does not appear to exist")
        options.vendorID = None
        return None
    data = json_file.readlines()
    vendors_list = {}
    i = 0
    for vendor_o in data:
        i += 1
        p = json.loads(vendor_o)
        vendors_list[i] = (p['companyName'], p['oui'])

    return vendors_list


if __name__ == '__main__':
    options = arg()
    newmac = ""

    if options.showVendor:
        available_vendors = dump_files()
        if available_vendors:
            for vendor in available_vendors:
                print(vendor, ': ', available_vendors[vendor])
        exit(0)

    interface = options.interfacePars
    if not chek_network_interfaces(interface):
        print("Invalid network interfaces")
        exit(1)

    if options.newMacPars:
        newmac = options.newMacPars
    else:
        if options.vendorID:
            available_vendors = dump_files()
            newmac = generate_random_mac(options.vendorID, available_vendors)
        else:
            newmac = generate_random_mac()


    if not check_mac(newmac):
        print("Invalid mac address")
        exit(1)


    change_mac(interface, newmac)
    exit(0)
